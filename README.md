Instructions:

To run:

```
pip install -r requirements.txt
docker-compose up -d
export FLASK_APP=app.py
python -m flask run
```
Docker-compose is required in order to spin up the redis container

Due to intricacies in Flask and or my rustiness in using Python/environmental issues (my windows desktop crashed when turning on Hyper V to use Docker), I was unable to get the flask app to run in the docker container alongside redis. Here's how the flow would go:

```
docker network create snaptravel # this creates a container-to-container network so that the flask app can reference the redis container using "redis" as the IP
docker-compose up --build -d # Build the python image from Dockerfile and run both snaptravel and redis as a daemon
```

You can try uncommenting the commented out lines (in docker-compose.yml) and then running those commands, it may work on your machine :)