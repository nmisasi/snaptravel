FROM python:2.7-alpine

WORKDIR /usr/src/app
EXPOSE 5000
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
ENV FLASK_APP=app.py
COPY . .

CMD [ "python", "-m", "flask", "run" ]