#!/usr/bin/python
from flask import Flask, render_template, request, make_response, jsonify
import requests
import redis
import json
from multiprocessing.pool import ThreadPool
SNAPTRAVEL = 'snaptravel'
RETAIL = 'retail'


r = redis.Redis(host='snaptravel', port=6379, db=0)

app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_template('input.html', name='world')

def makePostRequest(body):
    url = 'https://experimentation.snaptravel.com/interview/hotels'
    response = requests.post(url, data=body)
    result = dict()
    # This will make it easier to determine which source the results are coming from
    result[body['provider']] = response.json() 
    return result

def getResultIntersection(snapTravelData, retailData):
    intersection = []
    # TODO: See if I can make this more efficient
    for snapTravelHotel in snapTravelData['hotels']:
        for retailHotel in retailData['hotels']:
            if snapTravelHotel['id'] == retailHotel['id']:
                resultObject = dict()
                resultObject = snapTravelHotel.copy()
                resultObject['retailPrice'] = retailHotel['price']
                intersection.append(resultObject)
    return intersection

@app.route('/search', methods=['POST'])
def search():
    print(request.form)
    city = request.form["city"]
    checkin = request.form['checkIn']
    checkout = request.form['checkOut']
    # TODO: look up the city:checkin:checkout:provider key in redis
    redisKeySnapTravel = city + ':' + checkin + ':' + checkout + ':' + SNAPTRAVEL
    redisKeyRetail = city + ':' + checkin + ':' + checkout + ':' + RETAIL
    snapTravelData = None
    retailData = None
    snapTravelData = r.get(redisKeySnapTravel)
    retailData = r.get(redisKeyRetail)
    
    # If one isn't cached, grab both, in case data has changed in both places
    if snapTravelData is None or retailData is None:
        snapTravelBody = {
            'city': city,
            'checkin': checkin,
            'checkout': checkout,
            'provider': SNAPTRAVEL
        }
        retailBody = {
            'city': city,
            'checkin': checkin,
            'checkout': checkout,
            'provider': RETAIL
        }
        bodies = [snapTravelBody, retailBody]
        results = ThreadPool(2).imap(makePostRequest, bodies)
        for item in results:
            # We can use the key of the result to determine which result we are looking at
            if SNAPTRAVEL in item:
                snapTravelData = item[SNAPTRAVEL]
            else:
                retailData = item[RETAIL]

        # We cache the responses utilizing city:checkin:checkout:provider as the key
        r.set(redisKeySnapTravel, json.dumps(snapTravelData))
        r.set(redisKeyRetail, json.dumps(retailData))
        r.rpush('cachedkeys', redisKeyRetail)
        r.rpush('cachedkeys', redisKeySnapTravel)
    else:
        snapTravelData = json.loads(snapTravelData)
        retailData = json.loads(retailData)

    intersection = getResultIntersection(snapTravelData, retailData)
    return render_template('results.html', name='Results', data=intersection)


#Utility route for testing. Hit this with a get request and it will clear all the keys in the redis cache
@app.route('/clearCache', methods=['GET'])
def clearRedisCache():
    cachedkeys = r.lrange('cachedkeys', 0, -1)
    print(cachedkeys)
    for key in cachedkeys:
        r.delete(key)
    r.delete('cachedkeys')
    data = {'cachedkeys': cachedkeys}
    
    return make_response(jsonify(data), 200)